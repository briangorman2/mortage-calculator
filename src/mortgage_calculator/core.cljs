(ns mortgage_calculator.core
  (:require [rum.core :as rum]))

(defonce app-state (atom {:text "Hello world!"}))

(defonce principle (atom 100))
(defonce years (atom 30))
(defonce rate (atom 3.5))

(rum/defc principle-view < rum/reactive []
  []
  [:form
   "Principle" [:br]
   [:input {:type "text"
            :value (rum/react principle)
            :on-change #(reset! principle (.. % -target -value))}]])

(rum/defc years-view < rum/reactive []
  []
  [:form
   "Loan Years" [:br]
   [:input {:type "text"
            :value (rum/react years)
            :on-change #(reset! years (int (.. % -target -value)))}]])

(rum/defc rate-view < rum/reactive []
  []
  [:form
   "Interest Rate:" [:br]
   [:input {:type "text"
            :value (rum/react rate)
            :on-change #(reset! rate (.. % -target -value))}]])

(defn exponent
  [x n]
  (reduce * (repeat n x)))

(rum/defc total-payment-view < rum/reactive []
  [:div
   "Total Payment: " (int (* (rum/react principle) (exponent (+ 1 (/ (rum/react rate) 100)) (rum/react years))))])
 
(rum/defc monthly-payment-view < rum/reactive []
  [:div
   "Monthly Payment: " (int (/ (* (rum/react principle) (exponent (+ 1 (/ (rum/react rate) 100)) (rum/react years))) (* 12 (rum/react years))))])

(rum/defc mortgage-calc < rum/reactive []
    [:div
     (principle-view)
     (rate-view)
     (years-view)
     (total-payment-view)
     (monthly-payment-view)])

(rum/mount (mortgage-calc)
           (. js/document (getElementById "app")))

